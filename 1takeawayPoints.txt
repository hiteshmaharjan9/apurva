to create package.json => npm init -y
to run express app => npm run start

C = Create = Post
R = Read   = Get
U = Update = Patch/Put
D = Delete = Delete

making API
//frontend requests backend, backend gives response according to that request.
//This is called making API.

Postman Commands
//ctrl + E = Rename

//API hit

json()
Always place expressApp.use(json()) at top as only code that is written below it can use it 


//url = localhost:8000/name?address=lalitpur&isMarried=false
//url = route?query
//query = address=lalitpur&isMarried=false

//route = localhost:8000/name/a/b
//route = baseURL/routeParameter1/routeParameter2/routeParameter3
//baseURL = localhost:8000
//routeParameter1 = name
//routeParameter2 = a
//routeParameter3 = b

//here we have,
//static route parameter they are: name, a
//dynamic route parameter they are: product, b
//in place of dynamic route parameter we can write any
//.route("/name/:product/a/:b")

//3 ways to send data while sending request
//1. through query
//2. through the body of request
//3. through dynamic parameter

[1,2,3, [12, 8], [13, [5, 8]]]

//expressApp.use("/bikes", bikeRouter);

//What is middleware?
=> Middleware is function with req, res, and next as parameters.

=> To trigger another middleware we have to call next()

=> divided into two parameters
    => normal middleware
    (req, res, next) => {}
    to trigger next middleware we have to call next()
    => error middleware
    (err, req, res, next) => {}
    to trigger next middleware we have to call next("a")

=> each post, get, patch, delete are separate APIs