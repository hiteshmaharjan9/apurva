//make an express application
//attach port to that application

import express, { json } from "express";
import firstRouter from "./src/router/firstRouter.js";
import secondRouter from "./src/router/secondRouter.js";
import thirdRouter from "./src/router/thirdRouter.js";
import bikeRouter from "./src/router/bikeRouter.js";
import traineeRouter from "./src/router/traineeRouter.js";
import schoolRouter from "./src/router/schoolRouter.js";
import vehicleRouter from "./src/router/vehicleRouter.js";
import practiceMiddle from "./src/router/practiceMiddle.js";

let expressApp = express();
expressApp.use(json());

expressApp.listen(8000, () => {
    console.log("Express application is listening at port 8000");
});

expressApp.use("/product",firstRouter);
expressApp.use(secondRouter);
expressApp.use(thirdRouter);
expressApp.use("/bikes", bikeRouter);
expressApp.use("/trainees", traineeRouter);
expressApp.use("/schools", schoolRouter);
expressApp.use("/vehicles",  vehicleRouter);
expressApp.use("/",practiceMiddle);

//middleware