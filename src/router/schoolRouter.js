/*
url=localhost:8000/schools,post at response {success:true, message:"school created successfully"}
url=localhost:8000/schools,get at response {success:true, message:"school read successfully"}
url=localhost:8000/schools,patch at response {success:true, message:"school updated successfully"}
url=localhost:8000/schools,delete at response {success:true, message:"school deletedsuccessfully"}
*/

import {Router} from "express";

let schoolRouter = Router();

schoolRouter
.route("/")
.post((req, res, next) => {
    res.json({
        success: true,
        message: "school created successfully"
    });
})
.get((req, res, next) => {
    res.json({
        success: true,
        message: "school read successfully"
    })
})
.patch((req, res, next) => {
    res.json({
        success: true,
        message: "school updated successfully"
    })
})
.delete((req, res, next) => {
    res.json({
        success: true,
        message: "school deleted successfully"
    })
});
export default schoolRouter;