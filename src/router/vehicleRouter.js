/*
url=localhost:8000/vehicles,post at response {success:true, message:"vehicles created successfully"}
url=localhost:8000/vehicles,get at response {success:true, message:"vehicles read successfully"}
url=localhost:8000/vehicles,patch at response {success:true, message:"vehicles updated successfully"}
url=localhost:8000/vehicles,delete at response {success:true, message:"vehicles deletedsuccessfully"}
*/

import { Router } from "express";


let vehicleRouter = Router();

vehicleRouter
.route("/")
.post((req, res, next) => {
    res.json({
        success: true,
        message: "vehicles created successfully"
    });
})
.get((req, res, next) => {
    res.json({
        success: true,
        message: "vehicles read successfully"
    })
})
.patch((req, res, next) => {
    res.json({
        success:true,
        message: "vechicles updated successfully"
    })
})
.delete((req, res, next) => {
    res.json({
        success: true,
        message: "vehicle deleted successfully"
    })
});

export default vehicleRouter;