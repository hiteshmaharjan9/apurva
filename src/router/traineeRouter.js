import { Router } from "express";

let traineeRouter = Router();

traineeRouter
.route("/")
.post((req, res, next) => {
    let obj = {
        success: true,
        message: "trainees created successfully"
    };
    res.json(obj);
})
.get((req, res, next) => {
    let obj = {
        success: true,
        message: "trainees read successfully"
    };
    res.json(obj);
})
.patch((req, res, next) => {
    let obj = {
        success: true,
        message: "trainees updated successfully"
    };
    res.json(obj);
})
.delete((req, res, next) => {
    res.json({
        success: true,
        message: "trainees deleted successfully"
    })
})

export default traineeRouter;