import { Router } from "express";

let firstRouter = Router();

firstRouter
.route("/") // localhost:8000/product
.post((req, res)=>{
    // console.log("Home post");
    res.json("home post");
    // console.log(req.body);
    // console.log(req.query);
    // let obj = req.body;
    // console.log(obj.name);
})
.get((req, res)=>{
    res.json("Home get");
})
.patch((req, res) => {
        // console.log("Home patch");
        res.json("home patch");
})
.delete((req, res) => {
    // console.log("Home delete");
    res.json("home delete");
})


firstRouter
//here we have,
//static route parameter they are: name
//dynamic route parameter they are: product
//in place of dynamic route parameter we can write any
.route("/product/:name/a/:b")
.post((req, res) => {
    //req.params gives the dynamic route parameter
    console.log(req.params)
    res.json("Learning static route parameter and dynamic route parameter");
})


export default firstRouter;


//url = localhost:8000, POST then respond with "home post"
//url = localhost:8000, GET then respond with "home get"
//url = localhost:8000, PATCH then respond with "home patch"
//url = localhost:8000, DELETE then respond with "home delete"

//url = localhost:8000/name, POST then respond with "name post"
//url = localhost:8000/name, GET then respond with "name get"
//url = localhost:8000/name, PATCH then respond with "name patch"
//url = localhost:8000/name, DELETE then respond with "name delete"




//making API
//defining task for each request is called making API.

//postman = testing APIs
//the job of backend is to define  API

//put vs patch