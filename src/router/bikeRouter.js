import { Router } from "express";

let bikeRouter = Router();

bikeRouter
.route("/") //localhost:8000/bikes
.post((req, res, next) => {
    console.log("Middleware 1");
    next();
}, 
(err, req, res, next) => {
    console.log("Error Middleware 1");
    next()
},
(req, res, next) => {
    console.log("middleware 2");
    next("A");
},
(err, req, res, next) => {
    console.log("Error Middleware 2")
    next();
},
(req, res) => {
    console.log("middleware 3");
});

export default bikeRouter;